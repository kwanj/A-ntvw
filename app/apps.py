""" App module to bring together the whole app."""
from flask import Flask
from flask_restful import Api

from config import app_config
from app.views import PasswordChecker,MagicBall,TrippleCheck,AdditiveSeq,PgLatin



def create_app(config_name):
    """Create app function to create/bring together the app."""
    app = Flask(__name__)
    api = Api(app)
    app.config.from_object(app_config[config_name])
    app.url_map.strict_slashes = False
    api.add_resource(PasswordChecker, '/password')
    api.add_resource(MagicBall,'/magicball')
    api.add_resource(TrippleCheck,'/triple')
    api.add_resource(AdditiveSeq, '/addseq')
    api.add_resource(PgLatin, '/pglatin')
    return app
