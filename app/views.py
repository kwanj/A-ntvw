"""
File with Views/endpoints for the different Tasks.
"""
import re
import random
import string
import operator

from flask import request, json
from flask_restful import Resource

def space_stripper(data):
    """ Method to remove white space from inputs."""
    striped = "".join(data.split())
    return striped

class Validator(object):

    def __init__(self, password):
        self.password = password

    def check_for_symbol(self):
        if re.search("[@#$]+", self.password) is None:
            return False
        else:
            return True

    def check_for_lowcase(self):
        if re.search("[a-z]+", self.password) is None:
            return False
        else:
            return True

    def check_for_uppercase(self):
        if re.search("[A-Z]+", self.password) is None:
            return False
        else:
            return True

    def check_for_digits(self):
        if re.search("[0-9]+", self.password) is None:
            return False
        else:
            return True

    def check_length(self):
        l = len(self.password)
        if l >= 6 and l <= 12:
            return True
        else:
            return False


class PasswordChecker(Resource):
    """ A class Resource to create an passwordchecker endpoint."""
    def post(self):
        """ A Post Method that includes input validation."""
        data = request.get_json(force=True)
        pdata = data['password']
        my_list = pdata.split(",")
        i = 0
        passes = []
        while i < len(my_list):
            password = space_stripper(my_list[i])
            validator = Validator(password)
            symbol = validator.check_for_symbol()
            lowcase = validator.check_for_lowcase()
            uppercase = validator.check_for_uppercase()
            digits = validator.check_for_digits()
            length = validator.check_length()
            if symbol and lowcase and uppercase and digits and length:
                passes.append(password)
            i += 1
        if len(passes) > 0 :
            return {"status":"Success!","response":passes},200
        return {"status":"Failed!","message":"No Valid password found."},404

class MagicBall(Resource):

    def post(self):
        data = request.get_json(force=True)
        quiz = space_stripper(data['quiz'])
        if quiz == '':
            return { "status":"Failed!" ,"message":"Type in a question."},406

        answers = ['Yes','No',
                    'Depends','Maybe',
                    'Ask me Later','Ofcourse',
                    'Doubtful','Dunno','Ok',
                    'Why are u asking me this',
                    'Word','I know',
                    'Enough with the questions',
                    'God','Ask again',
                    'I can"t answer that ',
                    'Who knows....'
                    ]
        ansl = len(answers)-1
        r = answers[random.randint(0,ansl)]
        return {"status":"Success!", "message":r},200

class TrippleCheck(Resource):

    def post(self):
        data = request.get_json(force=True)
        nums = space_stripper(data['nums'])
        if nums == '':
            return {"status":"Failed!", "message":"Type in the numbers to be checked."},406
        a = nums.split(',')
        b = {}
        for i in a:
            if i in b:
                b[i]+=1
            else:
                b[i] = 1
        ls = []
        for k,v in b.items():
            if v < 3:
                ls.append(k)
        if len(ls) == 0:
            return {"status":"Failed!","message":"No number found."},404
        return {"status":"Success!", "numbers":ls},200

class AdditiveSeq(Resource):

    def post(self):
        data = request.get_json(force=True)
        seq = space_stripper(data['seq']).split(',')
        ses = [s for s in seq if s.isdigit()]
        if len(ses) != len(seq):
            return {"status":"Failed!","message":"Make sure sequence only contains integers."},406
        p = 0
        q = 1
        while q < len(seq)-1:
            if int(seq[p]) + int(seq[q]) == int(seq[q+1]):
                p += 1
                q += 1
            else:
                return {"status":"Failed!","message":False},404
        return {"status":"Success!","message":True},200

def check_for_q(data):
    b = "?"
    for char in b:
        a = data.replace(char,"")
        diff = len(data) - len(a)
        if diff == 3:
            return True
    if re.search('[?]{3}', data) is None:
        return False

class PgLatin(Resource):

    def post(self):
        data = request.get_json(force=True)
        instr =list(space_stripper(data['str']))
        intsList = list(string.digits)
        obj = filter(lambda x: operator.contains(intsList, x), instr)
        num = "".join(list(obj))
        nums = list(num)
        if len(instr) == 0:
            return {"status":"Failed!", "message":"Type in the string to be checked."},406
        i = 0
        j = i + 1
        # if i >= len(nums):
        #     return {"status":"Failed!","message":False},404
        for i in range(i,len(nums)):
            sum = int(nums[i]) + int(nums[j])
            t1 = nums[i]
            t2 = nums[j]
            while sum != 10:
                    i += 1
                    break
            if sum == 10:
                k=instr.index(t1)
                q=instr.index(t2)
                substr = instr[k:q]
                s1 = "".join(substr)
                if  not check_for_q(s1):
                    return {"status":"Failed!","message":False},404

        return {"status":"Success!","message":True},200