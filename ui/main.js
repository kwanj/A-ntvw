// Password Checker
let pass = document.getElementById('passchecker')
if (pass){
    pass.addEventListener
    ('submit', PassCheck);
}

function PassCheck(e){
    e.preventDefault();
    let passUrl = 'http://127.0.0.1:5000/password';
    let password = document.getElementById('password').value;

    fetch(passUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type':'application/json'
        },
        body:JSON.stringify({"password":password})
        })
        .then((res) => res.json())
        .then((data) => {
            let cls = document.getElementById('dcolor')
            let cls2 = document.getElementById('dmessage')
            if (data.status === 'Failed!'){
                // if request is unsuccessful
                cls.className = 'ui form error';
                cls2.className = 'ui error message'
                document.getElementById('headeroutput').innerHTML = data.status
                document.getElementById('output').innerHTML = data.message
            }
            if (data.status === "Success!"){
                cls.className = 'ui form success';
                cls2.className = 'ui success message'
                document.getElementById('headeroutput').innerHTML = data.status
                document.getElementById('output').innerHTML = data.response
            }
        })
        pass.reset();
    }

// Magic Ball
let progress = document.getElementById('prog')
let mb = document.getElementById('magicball')
progress.style.display = 'none';           // Hide
if (mb){
    mb.addEventListener
    ('submit', magicB);
}
function magicB(e){
    e.preventDefault();
    let passUrl = 'http://127.0.0.1:5000/magicball';
    let quiz = document.getElementById('quiz').value;
    progress.style.display = 'inline-block';   // Show

    setTimeout(function() {
        fetch(passUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-type':'application/json'
            },
            body:JSON.stringify({"quiz":quiz})
            })
                    
            .then((res) => res.json())
            .then((data) => {
                let cls = document.getElementById('dcolor1')
                let cls2 = document.getElementById('dmessage1')
                if (data.status === 'Failed!'){
                    // if request is unsuccessful
                    cls.className = 'ui form error';
                    cls2.className = 'ui error message'
                    progress.style.display = 'none';           // Hide
                    document.getElementById('headeroutput1').innerHTML = data.status
                    document.getElementById('output1').innerHTML = data.message
                }
                if (data.status === "Success!"){
                    cls.className = 'ui form success';
                    cls2.className = 'ui success message'
                    progress.style.display = 'none';           // Hide
                    document.getElementById('headeroutput1').innerHTML = data.status
                    document.getElementById('output1').innerHTML = data.message
                }
            })
            
    }, 2000);
    mb.reset();
    }


// Tripple elements checker
let tc = document.getElementById('triplec')
if (tc){
    tc.addEventListener
    ('submit', tripleC);
}

function tripleC(e){
    e.preventDefault();
    let passUrl = 'http://127.0.0.1:5000/triple';
    let nums = document.getElementById('nums').value;

    fetch(passUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type':'application/json'
        },
        body:JSON.stringify({"nums":nums})
        })
        .then((res) => res.json())
        .then((data) => {
            let cls = document.getElementById('dcolor2')
            let cls2 = document.getElementById('dmessage2')
            if (data.status === 'Failed!'){
                // if request is unsuccessful
                cls.className = 'ui form error';
                cls2.className = 'ui error message'
                document.getElementById('headeroutput2').innerHTML = data.status
                document.getElementById('output2').innerHTML = data.message
            }
            if (data.status === "Success!"){
                cls.className = 'ui form success';
                cls2.className = 'ui success message'
                document.getElementById('headeroutput2').innerHTML = data.status
                document.getElementById('output2').innerHTML = data.numbers
            }
        })
        tc.reset();
    }


// Additive sequence
let as = document.getElementById('addseq')
if (as){
    as.addEventListener
    ('submit', additiveSeq);
}

function additiveSeq(e){
    e.preventDefault();
    let passUrl = 'http://127.0.0.1:5000/addseq';
    let seq = document.getElementById('seq').value;

    fetch(passUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type':'application/json'
        },
        body:JSON.stringify({"seq":seq})
        })
        .then((res) => res.json())
        .then((data) => {
            let cls = document.getElementById('dcolor3')
            let cls2 = document.getElementById('dmessage3')
            if (data.status === 'Failed!'){
                // if request is unsuccessful
                cls.className = 'ui form error';
                cls2.className = 'ui error message'
                document.getElementById('headeroutput3').innerHTML = data.status
                document.getElementById('output3').innerHTML = data.message
            }
            if (data.status === "Success!"){
                cls.className = 'ui form success';
                cls2.className = 'ui success message'
                document.getElementById('headeroutput3').innerHTML = data.status
                document.getElementById('output3').innerHTML = data.message
            }
        })
        as.reset();
}

// Pig Latin
let pl = document.getElementById('pglatin')
if (pl){
    pl.addEventListener
    ('submit', pigLatin);
}
function pigLatin(e){
    e.preventDefault();
    let passUrl = 'http://127.0.0.1:5000/pglatin';
    let str = document.getElementById('str').value;

    fetch(passUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type':'application/json'
        },
        body:JSON.stringify({"str":str})
        })
        .then((res) => res.json())
        .then((data) => {
            let cls = document.getElementById('dcolor4')
            let cls2 = document.getElementById('dmessage4')
            if (data.status === 'Failed!'){
                // if request is unsuccessful
                cls.className = 'ui form error';
                cls2.className = 'ui error message'
                document.getElementById('headeroutput4').innerHTML = data.status
                document.getElementById('output4').innerHTML = data.message
            }
            if (data.status === "Success!"){
                cls.className = 'ui form success';
                cls2.className = 'ui success message'
                document.getElementById('headeroutput4').innerHTML = data.status
                document.getElementById('output4').innerHTML = data.message
            }
        })
        pl.reset();
}
