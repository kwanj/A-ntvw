import os

class Config(object):
    """Parent configuration class"""
    DEBUG = False

class DevelopmentConfig(Config):
    """Development configuration"""
    DEBUG = True

class TestingConfig(Config):
    """Development configuration"""
    DEBUG = True

app_config = {
    'development': DevelopmentConfig,
    'testing':TestingConfig
}