# A-ntvw
![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![Build Status](https://travis-ci.org/kwanj-k/A-ntvw.svg?branch=master)](https://travis-ci.org/kwanj-k/A-ntvw) [![Coverage Status](https://coveralls.io/repos/github/kwanj-k/A-ntvw/badge.svg?branch=master)](https://coveralls.io/github/kwanj-k/A-ntvw?branch=master) 

A set of tasks to test understanding of Python data structures, Algorithms and Test Driven Development

## Summary

This repo contains the logic and the UI for all the cycle 33 interview questions.
With this code,one should be able to:

```

1. Check the validity of password input by users.

2.PLay Magic 8 Ball game which is used for fortune-telling or seeking advice.

3. To find the single element in a list where every element appears three times except for one

4. To find whether a list contains an additive sequence or not.

5.Take an input string parameter and determine if exactly 3 question marks exist between every pair of numbers that add up to 10. If so, return true, otherwise return false.

```

## Getting Started 

* Clone the repository: 

    ```https://github.com/kwanj-k/A-ntvw.git```

* Navigate to the cloned repo. 

## Prerequisites

```- Python3```

## Installation and Usage

* After navigating to the cloned repo;

* Create a virtualenv and activate it with the following steps:

* Run the following on your terminal

    ``` 
    virtualenv -p python3 venv
    source venv/bin activate
    export FLASK_APP="run.py"
    export FLASK_DEBUG=1
    export APP_SETTINGS="development"```

Install the dependencies::

    pip install -r requirements.txt 

## Running tests with coverage

* Run:
```pytest --cov-report term-missing --cov=app```

## Testing on the UI

Run the development server:

  ```$ flask run```

Then open UI and try the different programs.

## Authors

* **Kelvin Mwangi** - *Initial work* - [kwanj-k](https://github.com/kwanj-k)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Contribution

Fork the repo, create a PR to this repository's develop.
