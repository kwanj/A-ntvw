import json

from tests.setting import Settings

pass_url = 'http://127.0.0.1:5000/magicball'

class TestMagic8Ball(Settings):

    g_p = {'quiz': 'Can you help me?'}
    b_p = {'quiz':''}

    def test_good_question(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.g_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)

    def test_empty_quiz(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.b_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 406)

    