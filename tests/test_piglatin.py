import json

from tests.setting import Settings

pass_url = 'http://127.0.0.1:5000/pglatin'

class TestPigLatin(Settings):

    g_p = {'str': '9jf??d?j8acc?7??sss?3rr1??????5'}
    b_p = {'str':'cc?7??sss?3rr1??????5'}
    empty = {'str':''}
    best  = {'str': 'hdfsh5?sa?5'}

    def test_qmark_available(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.g_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)
    
    def test_qmark_available2(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.b_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)

    def test_no_noqmark(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.best),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_empty_input(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.empty),
            content_type='application/json')
        self.assertEqual(res.status_code, 406)

    