import unittest
import json

from app.apps import create_app
import config


config_name = "testing"
app = create_app(config_name)


class Settings(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        