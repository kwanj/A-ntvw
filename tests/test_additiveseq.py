import json

from tests.setting import Settings

pass_url = 'http://127.0.0.1:5000/addseq'

class TestAdditive(Settings):

    g_p = {'seq': '0,1,1,2,3,5'}
    b_p = {'seq':'4,6,8'}
    int_d = {'seq':'4,6,f,t'}
    

    def test_seq_available(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.g_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)

    def test_no_seq(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.b_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_not_all_ints(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.int_d),
            content_type='application/json')
        self.assertEqual(res.status_code, 406)

    