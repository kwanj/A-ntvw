import json

from tests.setting import Settings

pass_url = 'http://127.0.0.1:5000/password'

class TestPasswordChecker(Settings):

    g_p = {'password': 'ABd1234@1,a F1#,2w3E*,2We3345'}
    b_p = {'password': 'a F1#,2w3E*,2We3345'}
    no_d = {'password': 'ABddfnn@,a F1#,2w3E*,2We3345'}
    no_u = {'password': 'hf87ddfnn@,a F1#,2w3E*,2We3345'}
    no_l = {'password': 'AAGDSHG87@,a F1#,2w3E*,2We3345'}

    def test_availabel_password(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.g_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)

    def test_no_valid_password(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.b_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)
    
    def test_no_digits_password(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.no_d),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_no_uppercase_password(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.no_u),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_no_lowcase_password(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.no_l),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    