import json

from tests.setting import Settings

pass_url = 'http://127.0.0.1:5000/triple'

class TestTripleChecker(Settings):

    g_p = {'nums': '3,5,7,3,9,3,5,'}
    b_p = {'nums':'3,3,3,8,8,8'}
    empty = {'nums': ''}

    def test_num_available(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.g_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 200)

    def test_no_num(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.b_p),
            content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_empty_input(self):
        res = self.app.post(pass_url,
            data=json.dumps(self.empty),
            content_type='application/json')
        self.assertEqual(res.status_code, 406)

    